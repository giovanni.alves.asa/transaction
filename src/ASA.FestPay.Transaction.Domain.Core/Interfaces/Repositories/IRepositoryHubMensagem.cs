﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryHubMensagem
    {
        public string Adicionar(HubMensagem model);
    }
}
