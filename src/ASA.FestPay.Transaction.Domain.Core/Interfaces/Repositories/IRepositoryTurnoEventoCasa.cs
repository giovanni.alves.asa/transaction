﻿using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryTurnoEventoCasa
    {
        TurnoEventoCasa ConsultarTurnoAtivoPorEvento(int eventoId);
    }
}
