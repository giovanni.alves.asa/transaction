﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryFormaPagamento : IRepositoryBase<FormaPagamento>
    {
        FormaPagamento ConsultarFormaPagamento(int? formaPagamentoId);
    }
}
