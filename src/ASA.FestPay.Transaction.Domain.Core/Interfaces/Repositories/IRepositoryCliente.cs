﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryCliente : IRepositoryBase<Cliente>
    {
        bool SalvarCliente(Cliente cliente, Pulseira pulseira, PulseiraSaldoEvento pulseiraSaldoEvento);

        Cliente ConsultarClientePorDocumento(string documento);
    }
}
