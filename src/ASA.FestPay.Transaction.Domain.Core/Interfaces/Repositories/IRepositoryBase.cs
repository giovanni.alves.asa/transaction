﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        int Adicionar(T model);

        T Consultar(int id);

        ICollection<T> Consultar();

        bool Atualizar(T model);
    }
}
