﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryPulseira : IRepositoryBase<Pulseira>
    {
        Pulseira ConsultarPulseira(string numeroControle);
        ICollection<Pulseira> ConsultarPulseirasBloqueadas(List<string> numeroControle);
    }
}
