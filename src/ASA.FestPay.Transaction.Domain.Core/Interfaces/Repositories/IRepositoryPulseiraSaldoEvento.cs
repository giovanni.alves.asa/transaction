﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryPulseiraSaldoEvento : IRepositoryBase<PulseiraSaldoEvento>
    {
        PulseiraSaldoEvento ConsultarPulseiraSaldoEvento(string numeroControlePulseira, int eventoId, List<string> comandasBloqueadas);

        ICollection<PulseiraSaldoEvento> ConsultarPulseiraClienteEvento(string documento, int eventoId, int turnoEventoCasaId);
    }
}
