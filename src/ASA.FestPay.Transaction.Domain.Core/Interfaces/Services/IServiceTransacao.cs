﻿using System;
using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServiceTransacao<T> where T : class
    {
        ICollection<Transacao> ConsultarTransacoesInseridas(Transacao model);

        ICollection<Transacao> ConsultarTransacoes(Transacao model);

        ICollection<TransacaoItem> ConsultarTransacoesItemInseridas(TransacaoItem model);

        ICollection<TransacaoIngresso> ConsultarTransacoesIngressoInseridas(TransacaoIngresso model);

        ICollection<TransacaoCliente> ConsultarTransacoesClienteInseridas(int eventoId, string numeroPulseira, decimal valor, DateTime dataTransacao, Guid transacaoIdHub);

        bool AdicionarTransacao(TransacaoParametrizada transacao);

        bool AtualizarTransacoes(ICollection<T> model);
    }
}