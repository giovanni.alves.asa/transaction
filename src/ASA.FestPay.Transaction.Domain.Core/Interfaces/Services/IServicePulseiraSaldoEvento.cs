﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServicePulseiraSaldoEvento : IServiceBase<PulseiraSaldoEvento>
    {
        PulseiraSaldoEvento ConsultarPulseiraSaldoEvento(string numeroControlePulseira, int eventoId, List<string> comandasBloqueadas);

        ICollection<PulseiraSaldoEvento> ConsultarPulseiraClienteEvento(string documento, int eventoId, int turnoEventoCasaId);
    }
}
