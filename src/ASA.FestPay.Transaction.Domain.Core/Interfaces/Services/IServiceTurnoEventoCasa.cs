﻿using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServiceTurnoEventoCasa
    {
        TurnoEventoCasa ConsultarTurnoAtivoPorEvento(int eventoId);
    }
}