﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IFestPayPainelApiService
    {
        public Task<UsuarioAutenticado> Autenticar(string login, string senha, bool manterConectado);
    }
}
