﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServicePulseira : IServiceBase<Pulseira>
    {
        Pulseira ConsultarPulseira(string numeroControle);
        ICollection<Pulseira> ConsultarPulseirasBloqueadas(List<string> numeroControle);
    }
}
