﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServiceFormaPagamento : IServiceBase<FormaPagamento>
    {
        FormaPagamento ConsultarFormaPagamento(int? formaPagamentoId);
    }
}
