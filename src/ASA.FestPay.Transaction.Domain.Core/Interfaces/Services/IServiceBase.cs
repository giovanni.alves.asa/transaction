﻿using System.Collections.Generic;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServiceBase<T>
    {
        ICollection<T> Consultar();
        T Consultar(int id);
        int Adicionar(T model);
        bool Atualizar(T model);
    }
}
