﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServiceCartaoBloqueado
    {
        public CartaoBloqueado ConsultarCartaoBloqueado(int eventoId, string numeroControle);
    }
}
