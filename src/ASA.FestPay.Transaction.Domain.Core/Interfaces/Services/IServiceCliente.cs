﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Services
{
    public interface IServiceCliente : IServiceBase<Cliente>
    {
        bool SalvarCliente(Cliente cliente, Pulseira pulseira, PulseiraSaldoEvento pulseiraSaldoEvento);

        Cliente ConsultarClientePorDocumento(string documento);

        Cliente ValidarCliente(string documento, string nome, string celular);
    }
}
