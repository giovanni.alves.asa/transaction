﻿using ASA.Festpay.Transaction.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace ASA.Festpay.Transaction.Infrastructure.Data
{
    public class FestPayTransacoesContext : DbContext
    {
        public DbSet<Transacao> Transacao { get; set; }
        public DbSet<TransacaoItem> TransacaoItem { get; set; }
        public DbSet<TransacaoCliente> TransacaoCliente { get; set; }
        public DbSet<TransacaoIngresso> TransacaoIngresso { get; set; }
        public DbSet<HubMensagem> HubMensagem { get; set; }
        public DbSet<Pulseira> Pulseira { get; set; }
        public DbSet<PulseiraSaldoEvento> PulseiraSaldoEvento { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Evento> Evento { get; set; }
        public DbSet<TurnoEventoCasa> TurnoEventoCasa { get; set; }
        public DbSet<CartaoBloqueado> CartaoBloqueado { get; set; }
        public DbSet<FormaPagamento> FormaPagamento { get; set; }

        public FestPayTransacoesContext() { }

        public FestPayTransacoesContext(DbContextOptions<FestPayTransacoesContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HubMensagem>().HasKey(x => x.MensagemId);
            modelBuilder.Entity<HubMensagem>().Property(x => x.MensagemId).HasColumnName("MensagemId");

            modelBuilder.Entity<CartaoBloqueado>().HasOne(cartao => cartao.Usuario).WithMany(usuario => usuario.CartaoBloqueado).HasForeignKey(cartao => cartao.UsuarioId);
            modelBuilder.Entity<CartaoBloqueado>().HasOne(cartao => cartao.Evento).WithMany(evento => evento.CartaoBloqueado).HasForeignKey(cartao => cartao.EventoId);

            modelBuilder.Entity<Transacao>().HasOne(tr => tr.FormaPagamento).WithMany(fp => fp.Transacao).HasForeignKey(tr => tr.FormaPagamentoId).OnDelete(DeleteBehavior.SetNull).IsRequired(false);
        }
    }
}