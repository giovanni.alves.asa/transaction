﻿using Microsoft.EntityFrameworkCore;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryCliente : RepositoryBase<Cliente>, IRepositoryCliente
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryCliente(FestPayTransacoesContext context) : base(context)
        {
            _context = context;
        }

        public Cliente ConsultarClientePorDocumento(string cpf)
        {
            return _context.Cliente.FirstOrDefault(f => f.Cpf.Equals(cpf));
        }

        public bool SalvarCliente(Cliente cliente, Pulseira pulseira, PulseiraSaldoEvento pulseiraSaldoEvento)
        {
            // using (var scope = _context.Database.BeginTransaction())
            // {
            // if (cliente.ClienteId == 0)
            // {
            //     var entityCliente = _context.Cliente.Add(cliente);
            //     if (entityCliente.State != EntityState.Added)
            //     {
            //         scope.Rollback();
            //         return false;
            //     }
            // }

            // var entityPulseira = _context.Pulseira.Add(pulseira);
            // if (entityPulseira.State != EntityState.Added)
            // {
            //     scope.Rollback();
            //     return false;
            // }

            var entityPulseiraSaldoEvento = _context.PulseiraSaldoEvento.Add(pulseiraSaldoEvento);
            if (entityPulseiraSaldoEvento.State != EntityState.Added)
            {
                // scope.Rollback();
                return false;
            }

            _context.SaveChanges();
            // scope.Commit();

            return true;
            // }
        }
    }
}