﻿using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using System.Threading.Tasks;
using System.Linq;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryCartaoBloqueado : IRepositoryCartaoBloqueado
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryCartaoBloqueado(FestPayTransacoesContext context)
        {
            _context = context;
        }

        public CartaoBloqueado ConsultarCartaoBloqueado(int eventoId, string numeroControle)
        {
            return _context.CartaoBloqueado
                           .Where(x => x.NumeroControle == numeroControle &&
                                       x.EventoId == eventoId).ToList().FirstOrDefault();
        }
    }
}