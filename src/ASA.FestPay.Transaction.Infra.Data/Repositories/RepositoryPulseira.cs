﻿using System.Linq;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using System.Collections.Generic;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryPulseira : RepositoryBase<Pulseira>, IRepositoryPulseira
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryPulseira(FestPayTransacoesContext context) : base(context)
        {
            _context = context;
        }

        public Pulseira ConsultarPulseira(string numeroControle)
        {
            return _context.Pulseira
                           .OrderBy(x => x.PulseiraId)
                           .FirstOrDefault(w => w.NumeroControle == numeroControle);
        }

        public ICollection<Pulseira> ConsultarPulseirasBloqueadas(List<string> pulseirasBloqueadas)
        {
            return _context.Pulseira
                           .Where(w => pulseirasBloqueadas.Contains(w.NumeroControle))
                           .ToList();
        }
    }
}