﻿using System.Linq;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryTurnoEventoCasa : IRepositoryTurnoEventoCasa
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryTurnoEventoCasa(FestPayTransacoesContext context)
        {
            _context = context;
        }

        public TurnoEventoCasa ConsultarTurnoAtivoPorEvento(int eventoId)
        {
            return _context.TurnoEventoCasa
                           .OrderByDescending(o => o.DataAbertura)
                           .FirstOrDefault(f => f.EventoId == eventoId && 
                                                f.DataFechamento == null && 
                                                f.DataAbertura != null);
        }
    }
}