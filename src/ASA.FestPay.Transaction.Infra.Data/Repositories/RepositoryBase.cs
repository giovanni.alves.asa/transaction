﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASA.Festpay.Transaction.Infrastructure.Data;

namespace ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryBase(FestPayTransacoesContext context)
        {
            _context = context;
        }

        public int Adicionar(T model)
        {
            var entity = _context.Set<T>().Add(model);
            return Convert.ToInt32(entity.CurrentValues["Id"]);
        }

        public bool Atualizar(T model)
        {
            _context.Set<T>().Update(model);
            return _context.SaveChanges() > 0;
        }

        public T Consultar(int id)
        {
            return _context.Find<T>(id);
        }

        public ICollection<T> Consultar()
        {
            return _context.Set<T>().ToList();
        }
    }
}