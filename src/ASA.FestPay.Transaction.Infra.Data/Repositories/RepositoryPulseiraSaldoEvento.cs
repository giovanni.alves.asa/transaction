﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using System.Collections.Generic;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryPulseiraSaldoEvento : RepositoryBase<PulseiraSaldoEvento>, IRepositoryPulseiraSaldoEvento
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryPulseiraSaldoEvento(FestPayTransacoesContext context) : base(context)
        {
            _context = context;
        }

        public ICollection<PulseiraSaldoEvento> ConsultarPulseiraClienteEvento(string documento, int eventoId, int turnoEventoCasaId)
        {
            return _context.PulseiraSaldoEvento
                           .Include(i => i.Pulseira)
                           .Include(i => i.Cliente)
                           .Include(i => i.Evento)
                           .ThenInclude(i => i.Transacao)
                           .Where(w => w.EventoId == eventoId &&
                                       w.Cliente.Cpf == documento &&
                                       w.Evento.Transacao.All(x => x.TurnoEventoCasa.TurnoEventoCasaId == turnoEventoCasaId))
                           .OrderBy(x => x.PulseiraId).ToList();
        }

        public PulseiraSaldoEvento ConsultarPulseiraSaldoEvento(string numeroControlePulseira, int eventoId, List<string> comandasBloqueadas)
        {
            return _context.PulseiraSaldoEvento
                           .Include(i => i.Cliente)
                           .Include(i => i.Pulseira)
                           .FirstOrDefault(f => f.Pulseira.NumeroControle == numeroControlePulseira &&
                                                f.EventoId == eventoId &&
                                                !comandasBloqueadas.Contains(f.Pulseira.NumeroControle));
        }
    }
}