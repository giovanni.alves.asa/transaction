﻿using System.Linq;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryFormaPagamento : RepositoryBase<FormaPagamento>, IRepositoryFormaPagamento
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryFormaPagamento(FestPayTransacoesContext context) : base(context)
        {
            _context = context;
        }

        public FormaPagamento ConsultarFormaPagamento(int? formaPagamentoId)
        {
            return _context.FormaPagamento
                           .FirstOrDefault(w => w.FormaPagamentoId == formaPagamentoId);
        }
    }
}