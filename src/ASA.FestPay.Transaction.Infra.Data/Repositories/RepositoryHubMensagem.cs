﻿using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using System.Threading.Tasks;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryHubMensagem : IRepositoryHubMensagem
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryHubMensagem(FestPayTransacoesContext context)
        {
            _context = context;
        }

        public string Adicionar(HubMensagem model)
        {
            var entity = _context.HubMensagem.Add(model);
            var ret = entity.CurrentValues["MensagemId"];
            return ret != null ? ret.ToString() : null;
        }
    }
}