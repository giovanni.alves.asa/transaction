﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infrastructure.Data;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Infra.Data.Repositories
{
    public class RepositoryTransacao<T> : IRepositoryTransacao<T> where T : class
    {
        private readonly FestPayTransacoesContext _context;

        public RepositoryTransacao(FestPayTransacoesContext context)
        {
            _context = context;
        }

        public ICollection<Transacao> ConsultarTransacoes(Transacao model)
        {
            return _context.Transacao
                           .Include(i => i.Usuario)
                           .Where(x => x.TransacaoIdHub == model.TransacaoIdHub &&
                                       x.HubId == model.HubId &&
                                       x.EventoId == model.EventoId)
                           .OrderByDescending(x => x.TransacaoId)?.ToList();
        }

        public ICollection<TransacaoItem> ConsultarTransacoesItemInseridas(TransacaoItem model)
        {
            return _context.TransacaoItem
                           .Include(i => i.Transacao)
                           .Where(w => w.Transacao.TransacaoId == model.Transacao.TransacaoId &&
                                       w.EventoId == model.EventoId &&
                                       w.HubId == model.HubId &&
                                       w.ProdutoId == model.ProdutoId &&
                                       w.ValorUnitario == model.ValorUnitario &&
                                       w.Quantidade == model.Quantidade).ToList();
        }

        public ICollection<TransacaoCliente> ConsultarTransacoesClienteInseridas(int eventoId,
                                                                                 string numeroPulseira,
                                                                                 decimal valor,
                                                                                 DateTime dataTransacao,
                                                                                 Guid transacaoIdHub)
        {
            return _context.TransacaoCliente
                           .Include(c => c.Transacao)
                           .Include(x => x.Pulseira)
                           .Where(x =>
                               x.EventoId == eventoId &&
                               x.Pulseira.NumeroControle == numeroPulseira &&
                               x.Valor == valor &&
                               // x.Transacao.DataTransacao == dataTransacao && 
                               x.TransacaoIdHub == transacaoIdHub
                            ).ToList();
        }


        public ICollection<Transacao> ConsultarTransacoesInseridas(Transacao model)
        {
            return _context.Transacao
                           .Where(w => w.HubId == model.HubId &&
                                       w.DataTransacao == model.DataTransacao &&
                                       w.TipoOperacao == (Enumeradores.TipoOperacao)model.TipoOperacao &&
                                       w.UsuarioId == model.UsuarioId &&
                                       w.PontoVendaId == model.PontoVendaId &&
                                       w.TransacaoIdHub == model.TransacaoIdHub).ToList();
        }

        public ICollection<TransacaoIngresso> ConsultarTransacoesIngressoInseridas(TransacaoIngresso model)
        {
            return _context.TransacaoIngresso
                           .Include(i => i.Transacao)
                           .Where(w => w.Transacao.EventoId == model.Transacao.EventoId &&
                                       w.Transacao.DataTransacao == model.DataTransacao &&
                                       w.TransacaoIdHub == model.TransacaoIdHub &&
                                       w.ValorBilheteria == model.ValorBilheteria &&
                                       w.ValorConsumacao == model.ValorConsumacao)
                           .ToList();
        }

        public bool AtualizarTransacoes(ICollection<T> model)
        {
            _context.Set<T>().UpdateRange(model);
            return _context.SaveChanges() > 0;
        }

        public bool AdicionarTransacao(TransacaoParametrizada transacao)
        {
            using (var scope = _context.Database.BeginTransaction())
            {
                      
                _context.HubMensagem.AddRange(transacao.Hub);

                var entityTransacao = _context.Set<Transacao>().Add(transacao.Transacao);
                if (entityTransacao.State != EntityState.Added)
                {
                    scope.Rollback();
                    return false;
                }

                if (transacao.Cliente != null && transacao.Pulseira != null && transacao.PulseiraSaldoEvento != null)
                {
                    var entityPulseiraSaldoEvento = _context.PulseiraSaldoEvento.Add(transacao.PulseiraSaldoEvento);
                    if (entityPulseiraSaldoEvento.State != EntityState.Added)
                    {
                        scope.Rollback();
                        return false;
                    }
                }

                _context.SaveChanges();
                scope.Commit();
                return true;
            }
        }
    }
}
