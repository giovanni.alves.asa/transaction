﻿namespace ASA.Festpay.Transaction.Infra.Data.API.Models
{
    /// <summary>
    /// Classe referente a exceções em requisições
    /// </summary>
    public class InternalServerErrorResponse
    {
        /// <summary>
        /// Mensagem
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Mensagem exceção
        /// </summary>
        public string ExceptionMessage { get; set; }
        /// <summary>
        /// Tipo de exceção
        /// </summary>
        public string ExceptionType { get; set; }
        /// <summary>
        /// StackTrace da exceção
        /// </summary>
        public string StackTrace { get; set; }
    }
}