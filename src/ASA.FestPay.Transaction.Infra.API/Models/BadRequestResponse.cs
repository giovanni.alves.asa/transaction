﻿namespace ASA.Festpay.Transaction.Infra.Data.API.Models
{
    /// <summary>
    /// Classe referente ao retorno de requisições em APIs
    /// </summary>
    public class BadRequestResponse
    {
        /// <summary>
        /// Mensagem
        /// </summary>
        public string Message { get; set; }
    }

}
