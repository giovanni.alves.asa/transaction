﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;

namespace ASA.Festpay.Transaction.Infra.Data.API.Models
{
    public class RetornoApi<T>
    {
        #region Atributos e propriedades

        /// <summary>
        /// Se a requisição foi sucesso ou não
        /// </summary>
        public bool Sucesso { get; set; }
        /// <summary>
        /// Código HTTP da requisição
        /// </summary>
        public int CodigoHTTP { get; set; }
        /// <summary>
        /// Os dados de retorno da requisição
        /// </summary>
        public T Dados { get; set; }
        /// <summary>
        /// Mensagem da requisição
        /// </summary>
        public string Mensagem { get; set; }
        /// <summary>
        /// Erro da requisição
        /// </summary>
        public InternalServerErrorResponse Erro { get; set; }

        #endregion

        #region Construtores

        /// <summary>
        /// Construtor
        /// </summary>
        public RetornoApi()
        {
            this.Erro = new InternalServerErrorResponse();
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Método para processar retorno da API
        /// </summary>
        /// <param name="response"></param>
        public void ProcessaRetorno(RestResponse response)
        {
            this.CodigoHTTP = (int)response.StatusCode;
            this.Sucesso = response.IsSuccessful;

            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.PartialContent)
            {
                this.Dados = JsonConvert.DeserializeObject<T>(response.Content);
            }
            else
            {
                this.Mensagem = MensagemHttp(response);
            }
        }

        /// <summary>
        /// Retorna mensagem do status code
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private string MensagemHttp(RestResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.NoContent:
                    return "Nenhum registro encontrado";
                case HttpStatusCode.NotFound:
                    return "Recurso não encontrado";
                case HttpStatusCode.MethodNotAllowed:
                    return "Método não permitido";
                case HttpStatusCode.BadRequest:
                    return JsonConvert.DeserializeObject<BadRequestResponse>(response.Content).Message;
                case HttpStatusCode.RequestTimeout:
                    return "A requisição expirou";
                case HttpStatusCode.Unauthorized:
                    return response.Content;
                case HttpStatusCode.InternalServerError:
                    return response.Content;
                case 0:
                    return "Falha ao se comunicar com a API";
                default:
                    throw new Exception($"{(int)response.StatusCode} - { response.StatusCode}");
            }
        }

        /// <summary>
        /// Processa exceção que aconteceu na requisição
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="mensagemPrefixo"></param>
        public void ProcessaExcecao(Exception ex, string mensagemPrefixo = null)
        {
            this.Sucesso = false;
            this.Mensagem = ex.Message;
            this.Erro.StackTrace = ex.StackTrace;

            if (ex.InnerException != null)
            {
                this.Erro.ExceptionMessage = ex.InnerException.ToString();
                this.Mensagem += this.Erro.ExceptionMessage;
            }

            if (mensagemPrefixo != null)
            {
                this.Mensagem = mensagemPrefixo + this.Mensagem;
            }
        }

        #endregion
    }
}