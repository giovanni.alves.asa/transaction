﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Infra.Data.API.Exceptions;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace ASA.Festpay.Transaction.Infra.Data.API
{
    public class FestPayPainelApiService : IFestPayPainelApiService
    {
        private IConfiguration _configuration;

        public FestPayPainelApiService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<UsuarioAutenticado> Autenticar(string login, string senha, bool manterConectado)
        {
            var host = _configuration.GetSection(this.GetType().Name).Value;
            var uri = "Usuarios/Autenticar";

            var api = new FestPayPainelApiRepository(host, uri, Method.Post);

            var retorno = await api.Execute<UsuarioAutenticado>(new { login = login, senha = senha, manterConectado = manterConectado });

            if (!retorno.Sucesso) 
            {
                throw new FestPayUnauthorizedException();
            }

            return retorno.Dados;
        }
    }
}
