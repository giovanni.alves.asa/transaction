﻿using ASA.Festpay.Transaction.Infra.Data.API.Models;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace ASA.Festpay.Transaction.Infra.Data.API
{
    public class ApiBaseRepository
    {
        #region ATRIBUTOS E PROPRIEDADES

        /// <summary>
        /// URL base da API
        /// </summary>
        protected string HOST { get; set; }
        /// <summary>
        /// Auth Token
        /// </summary>
        public string TOKEN { get; set; }
        /// <summary>
        /// HTTP Client
        /// </summary>
        private RestClient CLIENT { get; set; }
        /// <summary>
        /// HTTP REQUEST
        /// </summary>
        private RestRequest REQUEST { get; set; }

        #endregion

        #region CONSTRUTORES

        /// <summary>
        /// Construtor
        /// </summary>
        public ApiBaseRepository(string host,
                                 string uri,
                                 Method metodo = Method.Get,
                                 string token = null,
                                 string content = "application/json")
        {
            if (string.IsNullOrEmpty(host))
            {
                throw new Exception($"[!] API {this.GetType().Name} não configurada corretamente");
            }

            this.REQUEST = new RestRequest(uri, metodo);
            this.REQUEST.AddHeader("cache-control", "no-cache");
            this.REQUEST.AddHeader("Content-Type", content);
            this.REQUEST.AddHeader("Connection", "keep-alive");
            this.REQUEST.Timeout = int.MaxValue;

            if (!string.IsNullOrEmpty(token))
            {
                this.REQUEST.AddHeader("Authorization", $"{token}");
            }

            this.CLIENT = new RestClient(host);
        }

        #endregion

        #region METODOS

        /// <summary>
        /// Executar requisicao HTTP
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<RetornoApi<T>> Execute<T>(object data = null)
        {
            var retorno = new RetornoApi<T>();

            try
            {
                RestResponse retornoApi = null;

                if (data != null && this.REQUEST.Method != Method.Get)
                {
                    this.REQUEST.AddBody(data, "application/json");
                }

                retornoApi = await this.CLIENT.ExecuteAsync(this.REQUEST);

                if (retornoApi != null)
                {
                    retorno.ProcessaRetorno(retornoApi);
                }
                else
                {
                    throw new Exception("Falha ao processar requisição");
                }
            }
            catch (Exception ex)
            {
                retorno.ProcessaExcecao(ex);
            }

            return retorno;
        }

        #endregion
    }
}