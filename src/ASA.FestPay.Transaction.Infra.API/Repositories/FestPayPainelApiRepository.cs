﻿using RestSharp;

namespace ASA.Festpay.Transaction.Infra.Data.API
{
    public class FestPayPainelApiRepository : ApiBaseRepository
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="metodo"></param>
        public FestPayPainelApiRepository(string host, string uri, Method metodo, string token = null) : base(host, uri, metodo, token) { }
    }
}
