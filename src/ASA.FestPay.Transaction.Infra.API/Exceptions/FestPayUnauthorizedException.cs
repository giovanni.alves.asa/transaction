﻿using System;
namespace ASA.Festpay.Transaction.Infra.Data.API.Exceptions
{
    public class FestPayUnauthorizedException : Exception
    {
        public FestPayUnauthorizedException() : base("Usuário não autorizado") { }
    }
}
