﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ASA.Festpay.Transaction.Infra.Data.API
{
    /// <summary>
    /// Extensao de API
    /// </summary>

    public static class ApiExtension
    {
        /// <summary>
        /// Verifica se é um HttpStatusCode de sucesso
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool Sucesso(this HttpStatusCode code)
        {
            return (code == HttpStatusCode.OK || code == HttpStatusCode.Created ||
                code == HttpStatusCode.NoContent || code == HttpStatusCode.PartialContent);
        }

    }
}
