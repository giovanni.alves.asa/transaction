﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServiceFormaPagamento : ServiceBase<FormaPagamento>, IServiceFormaPagamento
    {
        private readonly IRepositoryFormaPagamento repository;

        public ServiceFormaPagamento(IRepositoryFormaPagamento repository) : base(repository)
        {
            this.repository = repository;
        }

        public FormaPagamento ConsultarFormaPagamento(int? formaPagamentoId) => repository.ConsultarFormaPagamento(formaPagamentoId);

    }
}