﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Entities;
using System;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServiceTransacao<T> : IServiceTransacao<T> where T : class
    {
        private readonly IRepositoryTransacao<T> repository;

        public ServiceTransacao(IRepositoryTransacao<T> repository)
        {
            this.repository = repository;
        }

        public ICollection<Transacao> ConsultarTransacoesInseridas(Transacao model) => repository.ConsultarTransacoesInseridas(model);

        public ICollection<Transacao> ConsultarTransacoes(Transacao model) => repository.ConsultarTransacoes(model);

        public ICollection<TransacaoItem> ConsultarTransacoesItemInseridas(TransacaoItem model) => repository.ConsultarTransacoesItemInseridas(model);

        public ICollection<TransacaoCliente> ConsultarTransacoesClienteInseridas(int eventoId, string numeroPulseira, decimal valor, DateTime dataTransacao, Guid transacaoIdHub) => repository.ConsultarTransacoesClienteInseridas(eventoId, numeroPulseira, valor, dataTransacao, transacaoIdHub);

        public ICollection<TransacaoIngresso> ConsultarTransacoesIngressoInseridas(TransacaoIngresso model) => repository.ConsultarTransacoesIngressoInseridas(model);

        public bool AdicionarTransacao(TransacaoParametrizada transacao) => repository.AdicionarTransacao(transacao);

        public bool AtualizarTransacoes(ICollection<T> model) => repository.AtualizarTransacoes(model);
    }
}