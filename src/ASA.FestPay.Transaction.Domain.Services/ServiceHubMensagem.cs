﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServiceHubMensagem : IServiceHubMensagem
    {
        private readonly IRepositoryHubMensagem repository;

        public ServiceHubMensagem(IRepositoryHubMensagem repository)
        {
            this.repository = repository;
        }

        public string Adicionar(HubMensagem model) => repository.Adicionar(model);
    }
}