﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServicePulseiraSaldoEvento : ServiceBase<PulseiraSaldoEvento>, IServicePulseiraSaldoEvento
    {
        private readonly IRepositoryPulseiraSaldoEvento repository;

        public ServicePulseiraSaldoEvento(IRepositoryPulseiraSaldoEvento repository) : base(repository)
        {
            this.repository = repository;
        }

        public ICollection<PulseiraSaldoEvento> ConsultarPulseiraClienteEvento(string documento, int eventoId, int turnoEventoCasaId) => repository.ConsultarPulseiraClienteEvento(documento, eventoId, turnoEventoCasaId);

        public PulseiraSaldoEvento ConsultarPulseiraSaldoEvento(string numeroControlePulseira, int eventoId, List<string> comandasBloqueadas) => repository.ConsultarPulseiraSaldoEvento(numeroControlePulseira, eventoId, comandasBloqueadas);
    }
}