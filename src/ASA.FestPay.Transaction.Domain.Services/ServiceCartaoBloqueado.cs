﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Entities;
using System;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServiceCartaoBloqueado : IServiceCartaoBloqueado
    {
        private readonly IRepositoryCartaoBloqueado repository;

        public ServiceCartaoBloqueado(IRepositoryCartaoBloqueado repository)
        {
            this.repository = repository;
        }

        public CartaoBloqueado ConsultarCartaoBloqueado(int eventoId, string numeroControle) => repository.ConsultarCartaoBloqueado(eventoId, numeroControle);
    }
}