﻿using System;
using ASA.Festpay.Transaction.Domain.Extensions;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServiceCliente : ServiceBase<Cliente>, IServiceCliente
    {
        private readonly IRepositoryCliente _repository;

        public ServiceCliente(IRepositoryCliente repository) : base(repository)
        {
            this._repository = repository;
        }

        public Cliente ConsultarClientePorDocumento(string documento) => _repository.ConsultarClientePorDocumento(documento);

        public bool SalvarCliente(Cliente cliente, Pulseira pulseira, PulseiraSaldoEvento pulseiraSaldoEvento) => _repository.SalvarCliente(cliente, pulseira, pulseiraSaldoEvento);

        public Cliente ValidarCliente(string documento, string nome, string celular)
        {
            if (!string.IsNullOrEmpty(documento))
            {
                bool alterado = false;
                var cliente = this.ConsultarClientePorDocumento(documento);
                if (cliente != null)
                {
                    if(!string.IsNullOrEmpty(nome) && 
                      ((!string.IsNullOrEmpty(cliente.Nome) && cliente.Nome.ToUpper().Contains("INFORMADO")) || 
                       string.IsNullOrEmpty(cliente.Nome)))
                    {
                        cliente.Nome = nome.Trim().ToUpper();
                        alterado = true;
                    }

                    if (!string.IsNullOrEmpty(celular) && cliente.Celular != celular)
                    {
                        cliente.Celular = celular.Trim();
                        alterado = true;
                    }

                    if (alterado)
                    {
                        cliente.DataModificacao = DateTime.Now.BrazilTZ();
                        this.Atualizar(cliente);
                    }

                    return cliente;
                }
            }

            return new Cliente(documento, nome, celular);
        }
    }
}