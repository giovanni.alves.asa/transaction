﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Entities;
using System;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServiceTurnoEventoCasa : IServiceTurnoEventoCasa
    {
        private readonly IRepositoryTurnoEventoCasa repository;

        public ServiceTurnoEventoCasa(IRepositoryTurnoEventoCasa repository)
        {
            this.repository = repository;
        }

        public TurnoEventoCasa ConsultarTurnoAtivoPorEvento(int eventoId) => repository.ConsultarTurnoAtivoPorEvento(eventoId);
    }
}