﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServiceBase<T> : IServiceBase<T> where T : class
    {
        private readonly IRepositoryBase<T> repository;

        public ServiceBase(IRepositoryBase<T> repository)
        {
            this.repository = repository;
        }

        public int Adicionar(T model) => repository.Adicionar(model);

        public bool Atualizar(T model) => repository.Atualizar(model);

        public T Consultar(int id) => repository.Consultar(id);

        public ICollection<T> Consultar() => repository.Consultar();
    }
}