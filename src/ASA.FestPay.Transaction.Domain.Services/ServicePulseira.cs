﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;

namespace ASA.Festpay.Transaction.Domain.Services
{
    public class ServicePulseira : ServiceBase<Pulseira>, IServicePulseira
    {
        private readonly IRepositoryPulseira repository;

        public ServicePulseira(IRepositoryPulseira repository) : base(repository)
        {
            this.repository = repository;
        }

        public Pulseira ConsultarPulseira(string numeroControle) => repository.ConsultarPulseira(numeroControle);

        public ICollection<Pulseira> ConsultarPulseirasBloqueadas(List<string> numeroControle) => repository.ConsultarPulseirasBloqueadas(numeroControle);
    }
}