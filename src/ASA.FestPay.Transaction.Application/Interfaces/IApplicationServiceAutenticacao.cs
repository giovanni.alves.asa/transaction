﻿using System.Threading.Tasks;
using ASA.Festpay.Transaction.Application.DTO;

namespace ASA.Festpay.Transaction.Application.Interfaces
{
    public interface IApplicationServiceAutenticacao
    {
        public Task<UsuarioAutenticadoDTO> Autenticar(string login, string senha, bool manterConectado);
    }
}
