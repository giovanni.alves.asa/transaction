﻿using ASA.Festpay.Transaction.Application.DTO;
using System.Collections.Generic;

namespace ASA.Festpay.Transaction.Application.Interfaces
{
    public interface IApplicationServiceTransacao
    {
        public bool ProcessarTransacoes(List<MensagemDTO> transacoes);
    }
}
