﻿using System.Collections.Generic;

namespace ASA.Festpay.Transaction.Application.Interfaces
{
    public interface IApplicationServiceBase<T> where T : class
    {
        ICollection<T> Consultar();
        T Consultar(int id);
        int Adicionar(T dto);
        bool Atualizar(T dto);
        bool Remover(int id);
    }
}
