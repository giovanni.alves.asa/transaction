﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.Logging;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Domain.Constants;
using ASA.Festpay.Transaction.Application.Interfaces;
using ASA.Festpay.Transaction.Application.DTO;
using ASA.Festpay.Transaction.Application.Exceptions;
using ASA.Festpay.Transaction.Application.Extensions;
using System.Collections.Generic;

namespace ASA.Festpay.Transaction.Application
{
    public class ApplicationServiceTransacao : IApplicationServiceTransacao
    {
        private readonly IServiceTransacao<Transacao> _serviceTransacao;
        private readonly IServiceTransacao<TransacaoCliente> _serviceTransacaoCliente;
        private readonly IServiceTransacao<TransacaoItem> _serviceTransacaoItem;
        private readonly IServiceTransacao<TransacaoIngresso> _serviceTransacaoIngresso;
        private readonly IServiceBase<Evento> _serviceEvento;
        private readonly IServiceCartaoBloqueado _serviceCartaoBloqueado;
        private readonly IServiceFormaPagamento _serviceFormaPagamento;
        private readonly IServiceHubMensagem _serviceHubMensagem;
        private readonly IServiceTurnoEventoCasa _serviceTurnoEventoCasa;
        private readonly IServicePulseiraSaldoEvento _servicePulseiraSaldoEvento;
        private readonly IServicePulseira _servicePulseira;
        private readonly IServiceCliente _serviceCliente;
        private readonly IMapper _mapper;
        private readonly ILogger<ApplicationServiceTransacao> _logger;

        public ApplicationServiceTransacao(IServiceTransacao<Transacao> serviceTransacao,
                                           IServiceTransacao<TransacaoItem> serviceTransacaoItem,
                                           IServiceTransacao<TransacaoCliente> serviceTransacaoCliente,
                                           IServiceTransacao<TransacaoIngresso> serviceTransacaoIngresso,
                                           IServiceHubMensagem serviceHubMensagem,
                                           IServiceTurnoEventoCasa serviceTurnoEventoCasa,
                                           IServiceBase<Evento> serviceEvento,
                                           IServiceCartaoBloqueado serviceCartaoBloqueado,
                                           IServicePulseiraSaldoEvento servicePulseiraSaldoEvento,
                                           IServiceFormaPagamento serviceFormaPagamento,
                                           IServicePulseira servicePulseira,
                                           IServiceCliente serviceCliente,
                                           ILogger<ApplicationServiceTransacao> logger,
                                           IMapper mapper)
        {
            _serviceTransacao = serviceTransacao;
            _serviceTransacaoCliente = serviceTransacaoCliente;
            _serviceTransacaoItem = serviceTransacaoItem;
            _serviceTransacaoIngresso = serviceTransacaoIngresso;
            _serviceHubMensagem = serviceHubMensagem;
            _serviceTurnoEventoCasa = serviceTurnoEventoCasa;
            _servicePulseiraSaldoEvento = servicePulseiraSaldoEvento;
            _serviceEvento = serviceEvento;
            _serviceCartaoBloqueado = serviceCartaoBloqueado;
            _serviceFormaPagamento = serviceFormaPagamento;
            _servicePulseira = servicePulseira;
            _serviceCliente = serviceCliente;
            _mapper = mapper;
            _logger = logger;
        }

        public void ProcessarMensagem(string mensagem, string id)
        {
            throw new NotImplementedException();
        }

        public bool ProcessarTransacoes(List<MensagemDTO> transacoes)
        {
            HubMensagem hubMensagem;
            DateTime dataAtual = DateTime.Now.BrazilTZ();
            var transacaoB = new Transacao();
            var transacaoParametrizada = new TransacaoParametrizada();
            transacaoParametrizada.Hub = new List<HubMensagem>();

            foreach (var item in transacoes)
            {
                string mensagem = item.Body;
                string origem = new TransacaoBaseDTO().CabecalhoMensagem(mensagem);

                switch (origem)
                {
                    case TipoTransacao.TR:
                        var transacao = _mapper.Map<TransacaoDTO, Transacao>(new TransacaoDTO(mensagem));

                        hubMensagem = new HubMensagem(transacao.HubId.Value, dataAtual, mensagem);
                        var transacaoDuplicada = ValidaTransacaoDuplicada(transacao);
                        if (!transacaoDuplicada)
                        {
                            var transacaoProcessada = ProcessarTransacao(transacao, hubMensagem);
                            transacaoParametrizada.Transacao = transacaoProcessada.Transacao;
                            transacaoParametrizada.Hub.Add(hubMensagem);

                            if (transacaoProcessada.Transacao.FormaPagamentoId == null)
                            {
                                transacaoParametrizada.Transacao.FormaPagamento = null;
                            }
                        }
                        else{ return true; }

                        break;

                    case TipoTransacao.TRCLI:
                        var dTO = new TransacaoClienteDTO(mensagem);
                        var transacaoCliente = _mapper.Map<TransacaoClienteDTO, TransacaoCliente>(dTO);
                        hubMensagem = new HubMensagem(transacaoCliente.HubId.Value, dataAtual, mensagem);
                        var transacaoClienteProcessada = ProcessarTransacao(transacaoCliente, dTO, hubMensagem, dataAtual);

                        transacaoParametrizada.Transacao.TransacaoCliente.Add(transacaoClienteProcessada.TransacaoCliente);
                        transacaoParametrizada.Hub.Add(hubMensagem);
                        transacaoParametrizada.Cliente = transacaoClienteProcessada.TransacaoCliente.Cliente;
                        transacaoParametrizada.Pulseira = transacaoClienteProcessada.TransacaoCliente.Pulseira;
                        transacaoParametrizada.PulseiraSaldoEvento = transacaoClienteProcessada.PulseiraSaldoEvento;
                        break;

                    case TipoTransacao.TRITEM:
                        var transacaoItem = _mapper.Map<TransacaoItemDTO, TransacaoItem>(new TransacaoItemDTO(mensagem));
                        hubMensagem = new HubMensagem(transacaoItem.HubId.Value, dataAtual, mensagem);
                        transacaoParametrizada.Transacao.TransacaoItem.Add(transacaoItem);
                        transacaoParametrizada.Hub.Add(hubMensagem);
                        break;

                    case TipoTransacao.TRING:
                        var dTOTRING = new TransacaoIngressoDTO(mensagem);
                        var transacaoIngresso = _mapper.Map<TransacaoIngressoDTO, TransacaoIngresso>(dTOTRING);
                        hubMensagem = new HubMensagem(dTOTRING.HubId.Value, dataAtual, mensagem);
                        transacaoParametrizada.Transacao.TransacaoIngresso.Add(transacaoIngresso);
                        transacaoParametrizada.Hub.Add(hubMensagem);
                        break;

                    default:
                        throw new FestPayTipoMensagemNaoIdentificado(mensagem);
                }
            }

            return _serviceTransacao.AdicionarTransacao(transacaoParametrizada);
        }


        private bool ValidaTransacaoDuplicada(Transacao transacao)
        {
            var transacaoDuplicada = _serviceTransacao.ConsultarTransacoesInseridas(transacao);
            if (transacaoDuplicada != null && transacao.Cancelada)
            {
                transacaoDuplicada.ToList().ForEach(tr => tr.Cancelada = true);
                if (!_serviceTransacao.AtualizarTransacoes(transacaoDuplicada))
                {
                    throw new FestPayMensagemNaoProcessadaException("Não foi possível atualizar o status da transação para cancelada.");
                }

                return true;
            }

            if (transacaoDuplicada == null && transacao.Cancelada)
            {
                throw new FestPayMensagemNaoProcessadaException();
            }

            return false;
        }

        private TransacaoParametrizada ProcessarTransacao(Transacao transacao, HubMensagem hub)
        {
            var transacaoParametrizada = new TransacaoParametrizada();

            transacao.FormaPagamento = null;
            var evento = _serviceEvento.Consultar(transacao.EventoId);
            if (evento.TipoEvento == (int)Enumeradores.TipoEvento.POS_PAGO)
            {
                var turno = _serviceTurnoEventoCasa.ConsultarTurnoAtivoPorEvento(evento.EventoId);
                if (turno == null)
                {
                    throw new FestPayMensagemNaoProcessadaException("TURNO ATIVO NAO ENCONTRADO");
                }
                transacao.TurnoEventoCasa = turno;
            }

            if (transacao.FormaPagamentoId.HasValue)
            {
                var fp = _serviceFormaPagamento.ConsultarFormaPagamento(transacao.FormaPagamentoId.Value);
                if (fp != null)
                {
                    transacao.FormaPagamento = fp;
                }
            }

            transacaoParametrizada.Transacao = transacao;
            return transacaoParametrizada;
        }

        private TransacaoClientePulseira ProcessarTransacao(TransacaoCliente transacaoCliente, TransacaoClienteDTO dTO, HubMensagem hub, DateTime dataAtual)
        {
            var comandaBloqueada = _serviceCartaoBloqueado.ConsultarCartaoBloqueado(dTO.EventoId, dTO.NumeroPulseira);

            if(comandaBloqueada != null)
            {
                throw new FestPayMensagemNaoProcessadaException("Essa comanda está bloqueada, não foi possível finalizar a transação.");
            }

            IEnumerable<CartaoBloqueado> comandasBloqueadasEvento = new List<CartaoBloqueado>();
            var pulseiraSaldoEvento = new PulseiraSaldoEvento();
            var transacaoDuplicada = _serviceTransacaoCliente.ConsultarTransacoesClienteInseridas(transacaoCliente.EventoId,
                                                                                                  dTO.NumeroPulseira,
                                                                                                  transacaoCliente.Valor,
                                                                                                  dTO.DataTransacao,
                                                                                                  transacaoCliente.TransacaoIdHub.Value);
            if (transacaoDuplicada != null && transacaoDuplicada.Any())
            {
                throw new FestPayMensagemNaoProcessadaException("Transação cliente já existe.");
            }

            var evento = _serviceEvento.Consultar(transacaoCliente.EventoId);
            if (evento == null)
            {
                throw new FestPayEventoNaoEncontradoException(transacaoCliente.EventoId);
            }

            var cliente = _serviceCliente.ValidarCliente(dTO.CpfCliente, dTO.NomeCliente, dTO.CelCliente);
            var pse = _servicePulseiraSaldoEvento.ConsultarPulseiraSaldoEvento(dTO.NumeroPulseira, evento.EventoId, comandasBloqueadasEvento.Select(s => s.NumeroControle).ToList());
            if (pse == null)
            {
                transacaoCliente.Pulseira = new Pulseira
                {
                    Cliente = cliente,
                    Bloqueada = false,
                    Vinculada = true,
                    DataCadastro = DateTime.Now.BrazilTZ(),
                    NumeroControle = dTO.NumeroPulseira,
                    DataImportacao = dataAtual,
                    DataModificacao = dataAtual
                };
                pulseiraSaldoEvento = new PulseiraSaldoEvento()
                {
                    Cliente = cliente,
                    Cpf = dTO.CpfCliente,
                    Celular = dTO.CelCliente,
                    Pulseira = transacaoCliente.Pulseira,
                    EventoId = evento.EventoId,
                    Saldo = 0,
                    DataCadastro = dataAtual
                };
                transacaoCliente.Cliente = cliente;
            }
            else
            {
                transacaoCliente.Cliente = pse.Cliente;
                transacaoCliente.Pulseira = pse.Pulseira;
            }

            transacaoCliente.EventoId = evento.EventoId;
            transacaoCliente.Cancelada = false;

            return new TransacaoClientePulseira()
            {
                TransacaoCliente = transacaoCliente,
                PulseiraSaldoEvento = pulseiraSaldoEvento
            };

        }
    }
}