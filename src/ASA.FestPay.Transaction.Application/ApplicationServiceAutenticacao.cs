﻿using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Application.Interfaces;
using AutoMapper;
using ASA.Festpay.Transaction.Application.DTO;
using System.Threading.Tasks;

namespace ASA.Festpay.Transaction.Application
{
    public class ApplicationServiceAutenticacao : IApplicationServiceAutenticacao
    {
        private readonly IFestPayPainelApiService _service;
        private readonly IMapper _mapper;

        public ApplicationServiceAutenticacao(IFestPayPainelApiService service,
                                              IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<UsuarioAutenticadoDTO> Autenticar(string login, string senha, bool manterConectado)
        {
            var model = await _service.Autenticar(login, senha, manterConectado);

            var dTO = _mapper.Map<UsuarioAutenticado, UsuarioAutenticadoDTO>(model);

            return dTO;
        }
    }
}