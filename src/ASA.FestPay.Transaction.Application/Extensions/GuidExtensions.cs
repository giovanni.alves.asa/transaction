using System;

namespace ASA.Festpay.Transaction.Application.Extensions
{
    public static class GuidExtensions
    {
        public static Guid TryToGuid(this string valor)
        {
            Guid.TryParse(valor, out var retorno);
            return retorno;
        }
    }
}