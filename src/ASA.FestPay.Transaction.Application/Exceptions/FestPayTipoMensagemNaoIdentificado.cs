using System;
using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Application.Exceptions
{
    public class FestPayTipoMensagemNaoIdentificado : FestPayBaseException
    {
        public FestPayTipoMensagemNaoIdentificado(string id) : base($"{MensagemRetorno.TIPO_TRANSACAO_NAO_IDENTIFICADO} [{id}]")  { }
    }
}