﻿using System;
using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Application.Exceptions
{
    public class FestPayMensagemNaoProcessadaException : FestPayBaseException
    {
        public FestPayMensagemNaoProcessadaException(string mensagem = null) : base($"{MensagemRetorno.NAO_PROCESSADO} [{mensagem}]")  { }
    }
}
