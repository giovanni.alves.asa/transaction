﻿using System;

namespace ASA.Festpay.Transaction.Application.Exceptions
{
    public class FestPayBaseException : Exception
    {
        public FestPayBaseException(string mensagem) : base(mensagem)  { }
    }
}
