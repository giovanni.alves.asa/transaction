﻿using System;
using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Application.Exceptions
{
    public class FestPayEventoNaoEncontradoException : FestPayBaseException
    {
        public FestPayEventoNaoEncontradoException(int? id = null) : base($"{MensagemRetorno.EVENTO_NAO_ENCONTRADO} [{id}]")  { }
    }
}
