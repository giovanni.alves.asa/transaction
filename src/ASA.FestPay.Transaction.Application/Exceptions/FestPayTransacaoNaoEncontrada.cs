using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Application.Exceptions
{
    public class FestPayTransacaoNaoEncontrada : FestPayBaseException
    {
        public FestPayTransacaoNaoEncontrada(string id, string tipo) : base($"[{tipo}] {MensagemRetorno.TRANSACAO_NAO_ENCONTRADA} [{id}]")  { }
    }
}