﻿using System.Globalization;
using ASA.Festpay.Transaction.Application.Extensions;

namespace ASA.Festpay.Transaction.Application.DTO
{
    public class TransacaoItemDTO : TransacaoBaseDTO
    {
        public int ProdutoId { get; set; }
        public decimal ValorUnitario { get; set; }
        public int Quantidade { get; set; }

        public TransacaoItemDTO() { }

        public TransacaoItemDTO(string mensagem)
        {
            var info = DeserializarMensagem(mensagem);

            this.HubId = 1;
            this.ProdutoId = int.Parse(info[4]);
            this.ValorUnitario = decimal.Parse(info[5], NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo);
            this.Quantidade = int.Parse(info[6]);
            this.TransacaoIdHub = info[3].TryToGuid();
        }
    }
}