﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using ASA.Festpay.Transaction.Application.Extensions;

namespace ASA.Festpay.Transaction.Application.DTO
{
    public class TransacaoClienteDTO : TransacaoBaseDTO
    {
        public string CpfCliente { get; set; }
        public string NomeCliente { get; set; }
        public string CelCliente { get; set; }
        public string DDDCelCliente { get; set; }
        public string UsuarioId { get; set; }
        public decimal Valor { get; set; }
        public string NumeroPulseira { get; set; }
        public decimal TaxaRateioPosPago { get; set; }

        [NotMapped]
        private DateTime _DataTransacao;
        public DateTime DataTransacao
        {
            get { return _DataTransacao; }
            set { _DataTransacao = value.TryUtcDate(); }
        }

        public TransacaoClienteDTO() { }

        public TransacaoClienteDTO(string mensagem)
        {
            var info = DeserializarMensagem(mensagem);
            var taxaRateio = 0.0M;

            if (!decimal.TryParse(info[7], NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out var valor))
                valor = 0;

            if (info.Length > 10 && !string.IsNullOrWhiteSpace(info[11]))
                this.DataTransacao = DateTime.Parse(info[11]);

            if (info.Length > 12 && !decimal.TryParse(info[12], NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out taxaRateio))
                taxaRateio = 0;

            this.HubId = 1;
            this.TransacaoIdHub = info[3].TryToGuid();
            this.CpfCliente = info[4];
            this.NomeCliente = info[5];
            this.CelCliente = info[6];
            this.Valor = valor;
            this.TaxaRateioPosPago = taxaRateio;
            this.NumeroPulseira = info[8];
            this.UsuarioId = info[9];
        }

        public override string ToString()
        {
            return "TRCLI" + "|" +
                EventoId + "|" +
                HubId + "|" +
                TransacaoIdHub + "|" +
                CpfCliente + "|" +
                NomeCliente + "|" +
                CelCliente + "|" +
                Valor.ToString().Replace(",", ".") + "|" +
                NumeroPulseira + "|" +
                UsuarioId + "|" +
                DataTransacao + "|" +
                TaxaRateioPosPago + "|";
        }
    }
}