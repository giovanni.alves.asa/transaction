﻿using System;
using System.Globalization;
using ASA.Festpay.Transaction.Application.Extensions;

namespace ASA.Festpay.Transaction.Application.DTO
{
    public class TransacaoIngressoDTO : TransacaoBaseDTO
    {
        public int TransacaoId { get; set; }
        public DateTime DataTransacao { get; set; }
        public decimal ValorBilheteria { get; set; }
        public decimal ValorConsumacao { get; set; }

        public TransacaoIngressoDTO() { }

        public TransacaoIngressoDTO(string mensagem)
        {
            var info = DeserializarMensagem(mensagem);

            this.HubId = 1;
            this.TransacaoIdHub = info[3].TryToGuid();
            this.DataTransacao = DateTime.ParseExact(info[4], "yyyy-MM-dd HH:mm:ss", null);

            if (!decimal.TryParse(info[5], NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out var valorBilheteria))
            {
                valorBilheteria = 0;
            }

            if (!decimal.TryParse(info[6], NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out var valorConsumacao))
            {
                valorConsumacao = 0;
            }

            this.ValorBilheteria = valorBilheteria;
            this.ValorConsumacao = valorConsumacao;
        }
    }
}