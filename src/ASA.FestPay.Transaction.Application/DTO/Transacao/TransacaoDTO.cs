﻿using System;
using ASA.Festpay.Transaction.Application.Extensions;

namespace ASA.Festpay.Transaction.Application.DTO
{
    public class TransacaoDTO : TransacaoBaseDTO
    {
        private DateTime _dataTransacao;
        public DateTime DataTransacao
        {
            get { return _dataTransacao; }
            set { _dataTransacao = value.TryUtcDate(); }
        }
        public int TipoOperacao { get; set; }
        public int UsuarioId { get; set; }
        public int? PontoVendaId { get; set; }
        public int? FormaPagamentoId { get; set; }
        public bool Cancelada { get; set; }

        public TransacaoDTO() { }

        public TransacaoDTO(string mensagem)
        {
            var info = DeserializarMensagem(mensagem);

            this.DataTransacao = DateTime.ParseExact(info[3], "yyyy-MM-dd HH:mm:ss", null);
            this.TipoOperacao = int.Parse(info[4]);
            this.UsuarioId = int.Parse(info[5]);
            this.PontoVendaId = info[6] == "null" ? (int?)null : int.Parse(info[6]);
            this.TransacaoIdHub = info[7].TryToGuid();
            this.Cancelada = info[8] == "1";

            if(info.Length > 9)
            {
                this.FormaPagamentoId = int.Parse(info[9]);
            }
        }
    }
}