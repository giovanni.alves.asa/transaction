using System;

namespace ASA.Festpay.Transaction.Application.DTO
{
    public class TransacaoBaseDTO
    {
        public int EventoId { get; set; }
        public int? HubId { get; set; }
        public Guid TransacaoIdHub { get; set; }
        public Guid RequestId { get; set; }
        public string TransacaoRecebida { get; set; }

        public string CabecalhoMensagem(string mensagem)
        {
            return ConteudoMensagem(mensagem)[0].ToUpper();
        }

        protected string[] ConteudoMensagem(string mensagem)
        {
            return mensagem.Trim("|".ToCharArray()).Split("|".ToCharArray());
        }

        protected string[] DeserializarMensagem(string mensagem)
        {
            this.RequestId = new Guid();
            this.TransacaoRecebida = mensagem;

            var info = ConteudoMensagem(mensagem);

            this.HubId = int.Parse(info[2]);
            this.EventoId = int.Parse(info[1]);

            return info;
        }
    }
}