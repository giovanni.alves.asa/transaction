﻿namespace ASA.Festpay.Transaction.Application.DTO
{
    public class UsuarioAutenticadoDTO
    {
        public string Id { get; set; }

        public int Codigo { get; set; }

        public string EstabelecimentoId { get; set; }

        public string Cpf { get; set; }

        public string Email { get; set; }

        public bool? Ativo { get; set; }

        public GenericResponseDTO PerfilAcesso { get; set; }

        public string NomeCompleto { get; set; }

        public string Token { get; set; }
    }

    public class GenericResponseDTO
    {
        public string Codigo { get; set; }

        public string Nome { get; set; }
    }
}