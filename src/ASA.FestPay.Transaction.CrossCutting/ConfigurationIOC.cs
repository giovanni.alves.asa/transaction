﻿using System.Collections.Generic;
using ASA.Festpay.Transaction.Application;
using ASA.Festpay.Transaction.Application.DTO;
using ASA.Festpay.Transaction.Application.Interfaces;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Repositories;
using ASA.Festpay.Transaction.Domain.Core.Interfaces.Services;
using ASA.Festpay.Transaction.Domain.Entities;
using ASA.Festpay.Transaction.Domain.Services;
using ASA.Festpay.Transaction.Infra.Data.API;
using ASA.Festpay.Transaction.Infra.Data.Repositories;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ASA.Festpay.Transaction.CrossCutting
{
    public class ConfigurationIOC
    {
        public static void LoadServices(IServiceCollection services)
        {

            services.AddScoped<IFestPayPainelApiService, FestPayPainelApiService>();
            services.AddScoped<IApplicationServiceAutenticacao, ApplicationServiceAutenticacao>();

            services.AddScoped<IApplicationServiceTransacao, ApplicationServiceTransacao>();

            services.AddScoped<IServiceTransacao<Transacao>, ServiceTransacao<Transacao>>();
            services.AddScoped<IRepositoryTransacao<Transacao>, RepositoryTransacao<Transacao>>();

            services.AddScoped<IServiceTransacao<TransacaoItem>, ServiceTransacao<TransacaoItem>>();
            services.AddScoped<IRepositoryTransacao<TransacaoItem>, RepositoryTransacao<TransacaoItem>>();

            services.AddScoped<IServiceTransacao<TransacaoCliente>, ServiceTransacao<TransacaoCliente>>();
            services.AddScoped<IRepositoryTransacao<TransacaoCliente>, RepositoryTransacao<TransacaoCliente>>();

            services.AddScoped<IServiceTransacao<TransacaoIngresso>, ServiceTransacao<TransacaoIngresso>>();
            services.AddScoped<IRepositoryTransacao<TransacaoIngresso>, RepositoryTransacao<TransacaoIngresso>>();

            services.AddScoped<IServiceHubMensagem, ServiceHubMensagem>();
            services.AddScoped<IRepositoryHubMensagem, RepositoryHubMensagem>();

            services.AddScoped<IServiceBase<Evento>, ServiceBase<Evento>>();
            services.AddScoped<IRepositoryBase<Evento>, RepositoryBase<Evento>>();

            services.AddScoped<IServiceBase<CartaoBloqueado>, ServiceBase<CartaoBloqueado>>();
            services.AddScoped<IRepositoryBase<CartaoBloqueado>, RepositoryBase<CartaoBloqueado>>();

            services.AddScoped<IServicePulseiraSaldoEvento, ServicePulseiraSaldoEvento>();
            services.AddScoped<IRepositoryPulseiraSaldoEvento, RepositoryPulseiraSaldoEvento>();

            services.AddScoped<IServicePulseira, ServicePulseira>();
            services.AddScoped<IRepositoryPulseira, RepositoryPulseira>();

            services.AddScoped<IServiceCliente, ServiceCliente>();
            services.AddScoped<IRepositoryCliente, RepositoryCliente>();

            services.AddScoped<IServiceTurnoEventoCasa, ServiceTurnoEventoCasa>();
            services.AddScoped<IRepositoryTurnoEventoCasa, RepositoryTurnoEventoCasa>();

            services.AddScoped<IServiceFormaPagamento, ServiceFormaPagamento>();
            services.AddScoped<IRepositoryFormaPagamento, RepositoryFormaPagamento>();

            services.AddScoped<IServiceCartaoBloqueado, ServiceCartaoBloqueado>();
            services.AddScoped<IRepositoryCartaoBloqueado, RepositoryCartaoBloqueado>();

            services.AddScoped<ILogger<ApplicationServiceTransacao>, Logger<ApplicationServiceTransacao>>();
        }

        public static void LoadMapper(IServiceCollection services)
        {
            var autoMapper = new MapperConfiguration(config =>
            {

                config.CreateMap<UsuarioAutenticado, UsuarioAutenticadoDTO>();
                config.CreateMap<UsuarioAutenticadoDTO, UsuarioAutenticado>();
                config.CreateMap<GenericResponse, GenericResponseDTO>();

                config.CreateMap<Transacao, TransacaoDTO>();
                config.CreateMap<TransacaoDTO, Transacao>();

                config.CreateMap<TransacaoCliente, TransacaoClienteDTO>();
                config.CreateMap<TransacaoClienteDTO, TransacaoCliente>();

                config.CreateMap<TransacaoItem, TransacaoItemDTO>();
                config.CreateMap<TransacaoItemDTO, TransacaoItem>();

                config.CreateMap<TransacaoIngresso, TransacaoIngressoDTO>();
                config.CreateMap<TransacaoIngressoDTO, TransacaoIngresso>();

            });

            IMapper mapper = autoMapper.CreateMapper();

            services.AddSingleton(mapper);
        }
    }
}