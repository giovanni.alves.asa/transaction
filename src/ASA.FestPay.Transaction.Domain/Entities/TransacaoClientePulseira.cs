using System;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class TransacaoClientePulseira
    {
        [Key]
        public TransacaoCliente TransacaoCliente { get; set; }
        public PulseiraSaldoEvento PulseiraSaldoEvento { get; set; }
    }
}