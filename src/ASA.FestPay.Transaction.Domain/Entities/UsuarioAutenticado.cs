﻿using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class UsuarioAutenticado
    {
        public string Id { get; set; }

        public int Codigo { get; set; }

        public string EstabelecimentoId { get; set; }

        public string Cpf { get; set; }

        public string Email { get; set; }

        public bool? Ativo { get; set; }

        public GenericResponse PerfilAcesso { get; set; }

        public string NomeCompleto { get; set; }

        public string Token { get; set; }
    }

    public class GenericResponse
    {
        [Key]
        public string Codigo { get; set; }

        public string Nome { get; set; }
    }
}