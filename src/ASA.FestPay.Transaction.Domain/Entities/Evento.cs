using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;
using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Evento
    {
        public Evento()
        {
            EstabelecimentoEvento = new List<EstabelecimentoEvento>();
            CartaoBloqueado = new List<CartaoBloqueado>();
            HubEvento = new List<HubEvento>();
            PontoVenda = new List<PontoVenda>();
            PulseiraSaldoEvento = new List<PulseiraSaldoEvento>();
            Transacao = new List<Transacao>();
            TransacaoItem = new List<TransacaoItem>();
            TransacaoCliente = new List<TransacaoCliente>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
            Sincronizado = false;
        }

        [Key]
        public int EventoId { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        public decimal TaxaServico { get; set; }
        public decimal ValorMinimo { get; set; }
        public decimal ValorMaximo { get; set; }
        public Enumeradores.TipoStatusEvento StatusEvento { get; set; }
        public Enumeradores.TipoStatusSincronizacaoEvento StatusSincronizacaoEvento { get; set; }
        public Enumeradores.TipoExibicaoCampo ExibicaoCPF { get; set; }
        public Enumeradores.TipoExibicaoCampo ExibicaoTelefone { get; set; }
        public int EnderecoId { get; set; }
        public byte[] BannerWeb { get; set; }
        public byte[] BannerApp { get; set; }
        public bool? DevolveSaldo { get; set; }
        public int? PercentualDevolucao { get; set; }
        public bool? Ativo { get; set; }
        public string LinkMapa { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool? EmDestaque { get; set; }
        public bool? VendaCreditoEventoAtualPermitida { get; set; }
        public bool Sincronizado { get; set; }
        public int TipoEvento { get; set; }
        public int TipoModuloEvento { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual ICollection<EstabelecimentoEvento> EstabelecimentoEvento { get; set; }
        public virtual ICollection<HubEvento> HubEvento { get; set; }
        public virtual ICollection<PontoVenda> PontoVenda { get; set; }
        public virtual ICollection<Transacao> Transacao { get; set; }
        public virtual ICollection<TransacaoItem> TransacaoItem { get; set; }
        public virtual ICollection<TransacaoCliente> TransacaoCliente { get; set; }
        public virtual ICollection<PulseiraSaldoEvento> PulseiraSaldoEvento { get; set; }
        public virtual ICollection<CartaoBloqueado> CartaoBloqueado { get; set; }

        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }

        public string SenhaNfc { get; set; }
        public string ChaveCriptografia { get; set; }
    }
}
