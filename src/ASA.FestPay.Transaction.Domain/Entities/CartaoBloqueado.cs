using System;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class CartaoBloqueado
    {
        public int Id { get; set; }
        public int EventoId { get; set; }
        public string NumeroControle { get; set; }
        public int UsuarioId { get; set; }
        public DateTime DataBloqueio { get; set; }
        public virtual Evento Evento { get; set; }
        public virtual Usuario Usuario { get; set; }

        public CartaoBloqueado() { }

        public CartaoBloqueado(Evento evento, Usuario usuario, string numeroControle)
        {
            this.NumeroControle = numeroControle;
            this.Evento = evento;
            this.UsuarioId = usuario.UsuarioId;
            this.DataBloqueio = DateTime.Now.BrazilTZ();
        }
    }
}