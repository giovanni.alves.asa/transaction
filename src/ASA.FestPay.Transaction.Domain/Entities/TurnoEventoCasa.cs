using System;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class TurnoEventoCasa
    {
        public int TurnoEventoCasaId { get; set; }       

        public string Descricao { get; set; }

        public int EventoId { get; set; }

        public virtual Evento Evento { get; set; }

        public DateTime? DataAberturaPrevisto { get; set; }

        public DateTime? DataFechamentoPrevisto { get; set; }

        public DateTime? DataAbertura { get; set; }

        public DateTime? DataFechamento { get; set; }

        public DateTime DataCadastro { get; set; }

        public int UsuarioIdAbertura { get; set; }

        public int? UsuarioIdFechamento { get; set; }

        public TurnoEventoCasa() { }

    }
}
