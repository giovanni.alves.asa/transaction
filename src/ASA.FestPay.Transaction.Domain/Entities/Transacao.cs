﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Transacao
    {
        public Transacao()
        {
            TransacaoItem = new List<TransacaoItem>();
            TransacaoCliente = new List<TransacaoCliente>();
            TransacaoIngresso = new List<TransacaoIngresso>();
            FormaPagamento = new FormaPagamento();
        }

        [Key]
        public int TransacaoId { get; set; }
        public Guid? TransacaoIdHub { get; set; }
        public DateTime DataTransacao { get; set; }
        public Enumeradores.TipoOperacao TipoOperacao { get; set; }
        public bool Cancelada { get; set; }
        public int? UsuarioId { get; set; }
        public int? PontoVendaId { get; set; }
        public int EventoId { get; set; }
        public int? FormaPagamentoId { get; set; }
        public int? HubId { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual PontoVenda PontoVenda { get; set; }
        public virtual Evento Evento { get; set; }
        public virtual Hub Hub { get; set; }
        public virtual TurnoEventoCasa TurnoEventoCasa { get; set; }
        public virtual FormaPagamento FormaPagamento { get; set; }

        public virtual ICollection<TransacaoCliente> TransacaoCliente { get; set; }
        public virtual ICollection<TransacaoItem> TransacaoItem { get; set; }
        public virtual ICollection<TransacaoIngresso> TransacaoIngresso { get; set; }

        public override string ToString()
        {
            return "TR|" +
                EventoId + "|" +
                HubId + "|" +
                DataTransacao.ToString("yyyy-MM-dd HH:mmm:ss") + "|" +
                TipoOperacao + "|" +
                UsuarioId + "|" +
                PontoVendaId + "|" +
                TransacaoIdHub + "|" +
                (Cancelada ? "1" : "0") + "|" +
                FormaPagamentoId;
        }
    }
}