using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Hub
    {
        public Hub()
        {
            HubEvento = new List<HubEvento>();
            Transacao = new List<Transacao>();
            TransacaoCliente = new List<TransacaoCliente>();
            TransacaoItem = new List<TransacaoItem>();
            HubMensagem = new List<HubMensagem>();
        }

        [Key]
        public int HubId { get; set; }
        public string Nome { get; set; }
        public string IdMachine { get; set; }
        public string Versao { get; set; }
        public string FilaEnvio { get; set; }
        public string FilaRecebimento { get; set; }
        public virtual ICollection<HubEvento> HubEvento { get; set; }
        public virtual ICollection<Transacao> Transacao { get; set; }
        public virtual ICollection<HubMensagem> HubMensagem { get; set; }
        public virtual ICollection<TransacaoItem> TransacaoItem { get; set; }
        public virtual ICollection<TransacaoCliente> TransacaoCliente { get; set; }
    }
}