using System;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class TransacaoItem
    {
        [Key]
        public int TransacaoItemId { get; set; }
        public Guid? TransacaoIdHub { get; set; }
        public int Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }
        public int ProdutoId { get; set; }
        public int EventoId { get; set; }
        public int? HubId { get; set; }
        public virtual Transacao Transacao { get; set; }
        public virtual Produto Produto { get; set; }
        public virtual Evento Evento { get; set; }
        public virtual Hub Hub { get; set; }

        public override string ToString()
        {
            return "TRITEM|" +
                 EventoId + "|" +
                 HubId + "|" +
                 TransacaoIdHub + "|" +
                 ProdutoId + "|" +
                 ValorUnitario.ToString().Replace(",", ".") + "|" +
                 Quantidade;
        }
    }
}