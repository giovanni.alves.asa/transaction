using System;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class EstabelecimentoEvento
    {
        public EstabelecimentoEvento()
        {
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
            Sincronizado = false;
        }

        [Key]
        public int EstabelecimentoId { get; set; }
        public int EventoId { get; set; }
        public decimal? TaxaServico { get; set; }
        public bool? Ativo { get; set; }
        public virtual Estabelecimento Estabelecimento { get; set; }
        public virtual Evento Evento { get; set; }
        public bool Sincronizado { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}
