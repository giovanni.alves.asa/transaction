using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Produto
    {
        public Produto()
        {
            ProdutoPontoVenda = new List<ProdutoPontoVenda>();
            TransacaoItem = new List<TransacaoItem>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
        }
        
        [Key]
        public int ProdutoId { get; set; }
        public string Nome { get; set; }
        public byte[] Foto { get; set; }
        public bool? Ativo { get; set; }
        public int CategoriaId { get; set; }
        public virtual Categoria Categoria { get; set; }
        public virtual ICollection<ProdutoPontoVenda> ProdutoPontoVenda { get; set; }
        public virtual ICollection<TransacaoItem> TransacaoItem { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}
