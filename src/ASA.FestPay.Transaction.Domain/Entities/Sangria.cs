using System;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Sangria
    {
        [Key]
        public int SangriaId { get; set; }

        public decimal Valor { get; set; }

        public bool Ativo { get; set; }

        public DateTime DataInclusao { get; set; }

        public DateTime? DataInativo { get; set; }

        public DateTime? DataAlteracao { get; set; }

        public int FormaPagamentoId { get; set; }

        public int EventoId { get; set; }

        public int UsuarioId { get; set; }

        public virtual FormaPagamento FormaPagamento { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual Evento Evento { get; set; }
    }
}