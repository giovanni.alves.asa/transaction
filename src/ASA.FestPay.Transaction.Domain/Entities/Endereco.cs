using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Endereco
    {
        public Endereco()
        {
            Cliente = new List<Cliente>();
            Estabelecimento = new List<Estabelecimento>();
            Evento = new List<Evento>();
        }

        [Key]
        public int EnderecoId { get; set; }
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public virtual ICollection<Cliente> Cliente { get; set; }
        public virtual ICollection<Estabelecimento> Estabelecimento { get; set; }
        public virtual ICollection<Evento> Evento { get; set; }
    }
}
