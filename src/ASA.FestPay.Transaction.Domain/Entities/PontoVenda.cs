using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class PontoVenda
    {
        public PontoVenda()
        {
            HubPontoVenda = new List<HubEvento>();
            PessoasCredenciadasEvento = new List<PessoasCredenciadasEvento>();
            ProdutoPontoVenda = new List<ProdutoPontoVenda>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
            Sincronizado = false;
        }

        [Key]
        public int PontoVendaId { get; set; }
        public string NomeFantasia { get; set; }
        public string NomeResponsavel { get; set; }
        public string Telefone { get; set; }
        public bool Ativo { get; set; }
        public int EstabelecimentoId { get; set; }
        public int EventoId { get; set; }
        public bool Sincronizado { get; set; }
        public virtual Estabelecimento Estabelecimento { get; set; }
        public virtual Evento Evento { get; set; }
        public virtual ICollection<HubEvento> HubPontoVenda { get; set; }
        public virtual ICollection<PessoasCredenciadasEvento> PessoasCredenciadasEvento { get; set; }
        public virtual ICollection<ProdutoPontoVenda> ProdutoPontoVenda { get; set; }
        public virtual ICollection<Transacao> Transacao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}