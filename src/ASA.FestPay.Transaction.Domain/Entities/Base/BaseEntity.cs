﻿using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities.Base
{
    public class BaseEntity
    {
        public BaseEntity() { }

        [Key]
        public int Id { get; set; }
    }
}
