using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class MensagemTransacoes
    {
        public MensagemTransacoes() { }

        [Key]
        public string Body { get; set; }
    }
}
