using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Pulseira
    {
        public Pulseira()
        {
            PulseiraSaldoEvento = new List<PulseiraSaldoEvento>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DateTime.Now.BrazilTZ();
            DataImportacao = DateTime.Now.BrazilTZ();
        }

        [Key]
        public int PulseiraId { get; set; }
        public string Apelido { get; set; }
        public bool Bloqueada { get; set; }
        public DateTime? DataImportacao { get; set; }
        public string NumeroControle { get; set; }
        public int? ClienteId { get; set; }
        public bool Vinculada { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual ICollection<PulseiraSaldoEvento> PulseiraSaldoEvento { get; set; }
        public virtual ICollection<TransacaoCliente> TransacaoCliente { get; set; }

        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}
