using System;
using ASA.Festpay.Transaction.Domain.Constants;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class HubMensagem
    {
        [Key]
        public Guid MensagemId { get; set; }
        public int HubId { get; set; }
        public DateTime MensagemData { get; set; }
        public string Mensagem { get; set; }
        public Enumeradores.TipoTransitoInformacao SentidoInformacao { get; set; }
        public virtual Hub Hub { get; set; }

        public HubMensagem() { }

        public HubMensagem(int hubId, DateTime dataMensagem, string mensagem)
        {
            this.MensagemId = Guid.NewGuid();
            this.HubId = hubId;
            this.MensagemData = dataMensagem;
            this.Mensagem = mensagem;
            this.SentidoInformacao = Enumeradores.TipoTransitoInformacao.Recebimento;
        }
    }
}