using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;
using ASA.Festpay.Transaction.Domain.Constants;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Usuario
    {
        public Usuario()
        {
            Estabelecimento = new List<Estabelecimento>();
            PessoasCredenciadasEvento = new List<PessoasCredenciadasEvento>();
            Sangria = new List<Sangria>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
        }

        [Key]
        public int UsuarioId { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public bool? Ativo { get; set; }
        public Enumeradores.TipoPerfilAcesso PerfilAcesso { get; set; }
        public int? EstabelecimentoIdPrincipal { get; set; }
        public string NomeCompleto { get; set; }
        public virtual ICollection<Estabelecimento> Estabelecimento { get; set; }
        public virtual ICollection<PessoasCredenciadasEvento> PessoasCredenciadasEvento { get; set; }
        public virtual ICollection<Transacao> Transacao { get; set; }
        public virtual ICollection<Sangria> Sangria { get; set; }
        public virtual ICollection<CartaoBloqueado> CartaoBloqueado { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
        public bool DesconsiderarRelatorio { get; set; }

    }
}
