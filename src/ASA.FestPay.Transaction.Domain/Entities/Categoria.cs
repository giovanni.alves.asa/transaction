using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Categoria
    {
        public Categoria()
        {
            Produto = new List<Produto>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
        }

        [Key]
        public int CategoriaId { get; set; }
        public string Descricao { get; set; }
        public virtual ICollection<Produto> Produto { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}
