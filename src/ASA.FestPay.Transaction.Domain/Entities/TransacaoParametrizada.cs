using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class TransacaoParametrizada
    {
        public TransacaoParametrizada() { }

        [Key]
        public Transacao Transacao { get; set; }
        public string TipoTransacao { get; set; }
        public List<HubMensagem> Hub { get; set; }
        public Cliente Cliente { get; set; }
        public Pulseira Pulseira { get; set; }
        public PulseiraSaldoEvento PulseiraSaldoEvento { get; set; }
    }
}
