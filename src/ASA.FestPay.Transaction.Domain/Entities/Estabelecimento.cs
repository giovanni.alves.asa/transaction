using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Estabelecimento
    {
        public Estabelecimento()
        {
            EstabelecimentoEvento = new List<EstabelecimentoEvento>();
            PontoVenda = new List<PontoVenda>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
        }

        [Key]
        public int EstabelecimentoId { get; set; }
        public string Cnpj { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string NomeResponsavel { get; set; }
        public bool Ativo { get; set; }
        public int UsuarioId { get; set; }
        public int? EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<EstabelecimentoEvento> EstabelecimentoEvento { get; set; }
        public virtual ICollection<PontoVenda> PontoVenda { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}
