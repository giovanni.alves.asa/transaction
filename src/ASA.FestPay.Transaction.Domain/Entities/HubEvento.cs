using System;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class HubEvento
    {
        [Key]
        public int HubId { get; set; }
        public int EventoId { get; set; }
        public DateTime UltimaAtualizacao { get; set; }
        public bool Ativo { get; set; }
        public string Ip { get; set; }
        public bool RecemAdicionado { get; set; }
        public virtual Evento Evento { get; set; }
        public virtual Hub Hub { get; set; }
    }
}