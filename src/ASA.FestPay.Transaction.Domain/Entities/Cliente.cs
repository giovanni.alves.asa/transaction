using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class Cliente
    {
        [Key]
        public int ClienteId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Cpf { get; set; }
        public string Celular { get; set; }
        public string Senha { get; set; }
        public DateTime DataCadastro { get; set; }
        public int? EnderecoId { get; set; }
        public int? FormaEntregaId { get; set; }
        public bool? Ativo { get; set; }
        public string GooglePlusKey { get; set; }
        public string FacebookKey { get; set; }
        public int? ConfiguracaoKm { get; set; }
        public bool? ConfiguracaoAlertarSaldo { get; set; }
        public bool? ConfiguracaoAvisarEvento { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual ICollection<Pulseira> Pulseira { get; set; }
        public virtual ICollection<PulseiraSaldoEvento> PulseiraSaldoEvento { get; set; }
        public virtual ICollection<TransacaoCliente> TransacaoCliente { get; set; }
        public DateTime DataModificacao { get; set; }

        public Cliente()
        {
            Pulseira = new List<Pulseira>();
            PulseiraSaldoEvento = new List<PulseiraSaldoEvento>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
        }

        public Cliente(string documento, string nome, string celular)
        {
            Ativo = true;
            Cpf = documento;
            Nome = string.IsNullOrEmpty((nome ?? "").Trim()) ? "NOME NÃO INFORMADO" : nome.ToUpper().Trim();
            Celular = celular;
            Pulseira = new List<Pulseira>();
            PulseiraSaldoEvento = new List<PulseiraSaldoEvento>();
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
        }
    }
}
