using System;
using System.ComponentModel.DataAnnotations;
using ASA.Festpay.Transaction.Domain.Extensions;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class ProdutoPontoVenda
    {
        public ProdutoPontoVenda()
        {
            DataCadastro = DateTime.Now.BrazilTZ();
            DataModificacao = DataCadastro;
            Sincronizado = false;
        }

        [Key]
        public int ProdutoPontoVendaId { get; set; }
        public decimal? Preco { get; set; }
        public int PontoVendaId { get; set; }
        public int ProdutoId { get; set; }
        public int EventoId { get; set; }
        public bool Ativo { get; set; }
        public bool Sincronizado { get; set; }
        public virtual Evento Evento { get; set; }
        public virtual PontoVenda PontoVenda { get; set; }
        public virtual Produto Produto { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}
