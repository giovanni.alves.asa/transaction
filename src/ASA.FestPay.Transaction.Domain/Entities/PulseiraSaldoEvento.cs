using System;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class PulseiraSaldoEvento
    {
        public PulseiraSaldoEvento()
        {
            Sincronizado = false;
        }
        
        [Key]
        public int PulseiraSaldoEventoId { get; set; }
        public int EventoId { get; set; }
        public int ClienteId { get; set; }
        public decimal? Saldo { get; set; }
        public int PulseiraId { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime? DataModificacao { get; set; }
        public bool Sincronizado { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Evento Evento { get; set; }
        public virtual Pulseira Pulseira { get; set; }
        public string Cpf { get; set; }
        public string Celular { get; set; }
    }
}
