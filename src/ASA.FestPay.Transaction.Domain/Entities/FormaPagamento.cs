using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class FormaPagamento
    {
        public FormaPagamento()
        {
            Sangria = new List<Sangria>();
            Transacao = new List<Transacao>();
        }

        [Key]
        public int FormaPagamentoId { get; set; }
        public string Descricao { get; set; }

        public bool Ativo { get; set; }

        public virtual ICollection<Sangria> Sangria { get; set; }
        public virtual ICollection<Transacao> Transacao { get; set; }
    }
}
