using System;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    // |TRING|{EventoId}|{TransacaoId}|{DataTransacao}|{ValorBilheteria}|{ValorConsumacao}|
    public class TransacaoIngresso
    {
        [Key]
        public int TransacaoIngressoId { get; set; }
        public Guid? TransacaoIdHub { get; set; }
        public int TransacaoId { get; set; }
        public DateTime DataTransacao { get; set; }
        public decimal ValorBilheteria { get; set; }
        public decimal ValorConsumacao { get; set; }

        public virtual Transacao Transacao { get; set; }
       
        public override string ToString()
        {
            return "TRING|" +
                 Transacao.EventoId + "|" +
                 TransacaoIdHub + "|" +
                 DataTransacao + "|" +
                 ValorBilheteria.ToString().Replace(",", ".") + "|" +
                 ValorConsumacao.ToString().Replace(",", ".") + "|";
        }
    }
}