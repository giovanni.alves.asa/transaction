using System;
using System.ComponentModel.DataAnnotations;

namespace ASA.Festpay.Transaction.Domain.Entities
{
    public class TransacaoCliente
    {
        [Key]
        public int TransacaoClienteId { get; set; }
        public Guid? TransacaoIdHub { get; set; }
        public decimal Valor { get; set; }
        public int TransacaoId { get; set; }
        public int ClienteId { get; set; }
        public int PulseiraId { get; set; }
        public int EventoId { get; set; }
        public int? HubId { get; set; }
        public decimal TaxaRateioPosPago { get; set; }
        public virtual Hub Hub { get; set; }
        public virtual Transacao Transacao { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Pulseira Pulseira { get; set; }
        public virtual Evento Evento { get; set; }
        public bool Cancelada { get; set; }
        public DateTime? DataCancelada { get; set; }
    }
}