﻿namespace ASA.Festpay.Transaction.Domain.Constants
{
    public static class MensagemRetorno
    {
        public static string NAO_PROCESSADO { get; set; } = "Mensagem não processada";
        public static string TRANSACAO_NAO_ENCONTRADA { get; set; } = "TR NÃO PROCESSADA";
        public static string TIPO_TRANSACAO_NAO_IDENTIFICADO { get; set; } = "Tipo de transação não identificado";
        public static string EVENTO_NAO_ENCONTRADO  { get; set; } = "Evento não encontrado";
    }
}