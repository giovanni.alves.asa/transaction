namespace ASA.Festpay.Transaction.Domain.Constants
{
    public static class TipoTransacao
    {
        public const string TR = "TR";
        public const string TRCLI = "TRCLI";
        public const string TRITEM = "TRITEM";
        public const string TRING = "TRING";
    }
}