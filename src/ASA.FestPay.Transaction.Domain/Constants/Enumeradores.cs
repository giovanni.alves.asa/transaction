using System.ComponentModel;

namespace ASA.Festpay.Transaction.Domain.Constants
{
    public static class Enumeradores
    {
        public enum TipoOperacao
        {
            Debito = 0,
            Credito = 1,
            Bloqueio = 2,
            Desbloqueio = 3,
            Vincular = 4,
            VincularNova = 5
        }

        public enum TipoTransitoInformacao
        {
            Envio = 1,
            Recebimento = 2
        }

        public enum TipoStatusEvento
        {
            [Description("Cancelado")]
            Cancelado = 1,
            [Description("Disponível")]
            Disponivel = 2,
            [Description("Pendente")]
            Pendende = 3
        }

        public enum TipoStatusSincronizacaoEvento
        {
            [Description("Não iniciada")]
            NaoIniciada = 1,
            [Description("Ativa")]
            Ativa = 2,
            [Description("Pausada")]
            Pausada = 3,
            [Description("Encerrada")]
            Encerrada = 4
        }

        public enum TipoExibicaoCampo
        {
            [Description("Não")]
            Nao = 1,

            [Description("Opcional")]
            SimOpcional = 2,

            [Description("Obrigatório")]
            SimObrigatorio = 3
        }

        public enum TipoPerfilAcesso
        {
            Nenhum = 0,
            Administrador = 1,
            Estabelecimento = 2,
            Vendedor = 3,
            Operador = 4,
            Produtor = 5
        }

        public enum TipoEvento
        {
            POS_PAGO = 1,
            PRE_PAGO = 2
        }
    }
}