using System;

namespace ASA.Festpay.Transaction.Domain.Extensions
{
    public static class Extensions
    {
        public static DateTime TryUtcDate(this DateTime date)
        {
            return new DateTime(year: date.Year, month: date.Month, day: date.Day, hour: date.Hour, minute: date.Minute, second: date.Second, kind: DateTimeKind.Utc);
        }

        public static DateTime BrazilTZ(this DateTime date)
        {
            try
            {
                return TimeZoneInfo.ConvertTime(date, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
            }
            catch (Exception)
            {
                return TimeZoneInfo.ConvertTime(date, TimeZoneInfo.FindSystemTimeZoneById("America/Sao_Paulo"));
            }
        }
    }
}
