using System;

namespace ASA.Festpay.Transaction.Domain.Extensions
{
    public static class GuidExtensions
    {
        public static Guid TryToGuid(this string valor)
        {
            Guid.TryParse(valor, out var retorno);
            return retorno;
        }
    }
}