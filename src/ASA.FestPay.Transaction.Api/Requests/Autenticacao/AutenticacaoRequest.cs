﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ASA.Festpay.Transaction.Api.Requests.Autenticacao
{
    [DataContract]
    public class AutenticacaoRequest
    {
        [DataMember(Name = "login")]
        [Required(ErrorMessage = "Login é obrigatório")]
        public string Login { get; set; }

        [DataMember(Name = "senha")]
        [Required(ErrorMessage = "Senha é obrigatório")]
        public string Senha { get; set; }

        [DataMember(Name = "manterConectado")]
        [Required(ErrorMessage = "Manter conectado é obrigatório")]
        public bool ManterConectado { get; set; }
    }
}