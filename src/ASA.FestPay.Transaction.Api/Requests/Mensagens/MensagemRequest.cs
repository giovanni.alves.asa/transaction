﻿using ASA.Festpay.Transaction.Application.DTO;
using System.Runtime.Serialization;

namespace ASA.Festpay.Transaction.Api.Requests.Autenticacao
{
    [DataContract]
    public class MensagemRequest
    {
        [DataMember(Name = "Body")]
        public string Body { get; set; }

        public MensagemDTO ConverterParaDTO()
        {
            return new MensagemDTO()
            {
                Body = this.Body,
            };
        }
    }
}