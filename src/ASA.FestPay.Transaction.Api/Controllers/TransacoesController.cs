﻿using System;
using System.Collections.Generic;
using System.Net;
using ASA.Festpay.Transaction.Api.Requests.Autenticacao;
using ASA.Festpay.Transaction.Application.DTO;
using ASA.Festpay.Transaction.Application.Exceptions;
using ASA.Festpay.Transaction.Application.Interfaces;
using ASA.Festpay.Transaction.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ASA.Festpay.Transaction.Api.Controllers
{
    [ApiController]
    [Authorize("Bearer")]
    [Route("[controller]")]
    public class TransacoesController : ControllerBase
    {
        private IApplicationServiceTransacao _applicationServiceTransacao;

        public TransacoesController(IApplicationServiceTransacao applicationServiceTransacao)
        {
            _applicationServiceTransacao = applicationServiceTransacao;
        }

        [HttpPost]
        public IActionResult Post(List<MensagemRequest> mensagens)
        {
            try
            {
                var mensagensDTO = new List<MensagemDTO>();
                
                foreach(var mensagem in mensagens)
                {
                    mensagensDTO.Add(mensagem.ConverterParaDTO());
                }

                var retorno = _applicationServiceTransacao.ProcessarTransacoes(mensagensDTO);

                return StatusCode((int)HttpStatusCode.OK, retorno);
            }
            catch (FestPayNoContentException e)
            {
                return StatusCode((int)HttpStatusCode.NoContent, e.Message);
            }
            catch (FestPayBaseException e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}