﻿using System;
using System.Runtime.Serialization;

namespace ASA.Festpay.Transaction.Api.Controllers
{
    [Serializable]
    internal class FestPayNoContentException : Exception
    {
        public FestPayNoContentException()
        {
        }

        public FestPayNoContentException(string message) : base(message)
        {
        }

        public FestPayNoContentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FestPayNoContentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}