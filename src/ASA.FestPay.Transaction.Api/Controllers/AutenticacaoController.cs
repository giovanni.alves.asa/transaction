﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using ASA.Festpay.Transaction.Api.Requests.Autenticacao;
using ASA.Festpay.Transaction.Api.Security;
using ASA.Festpay.Transaction.Application.Exceptions;
using ASA.Festpay.Transaction.Application.Interfaces;
using ASA.Festpay.Transaction.Infra.Data.API.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace ASA.Festpay.Transaction.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AutenticacaoController : ControllerBase
    {
        private IApplicationServiceAutenticacao _applicationServiceAutenticacao;
        private TokenConfigurations _tokenConfiguration;
        private SigningConfigurations _signingConfigurations;

        public AutenticacaoController(IApplicationServiceAutenticacao applicationServiceAutenticacao,
                                      TokenConfigurations tokenConfiguration,
                                      SigningConfigurations signingConfigurations)
        {
            _applicationServiceAutenticacao = applicationServiceAutenticacao;
            _tokenConfiguration = tokenConfiguration;
            _signingConfigurations = signingConfigurations;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AutenticacaoRequest model)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                var dataCriacao = DateTime.Now;
                var dataExpiracao = model.ManterConectado ?
                                    dataCriacao.AddDays(365) : dataCriacao.AddSeconds(_tokenConfiguration.Seconds);

                var retorno = await _applicationServiceAutenticacao.Autenticar(model.Login, model.Senha, model.ManterConectado);

                var token = handler.ReadJwtToken(retorno.Token);

                var usuarioId = token.Claims.First(x => x.Type == "usuarioId").Value;
                var permissoes = token.Claims.First(x => x.Type == "permissoes").Value;
                var claims = new ClaimsIdentity(
                    new GenericIdentity(usuarioId, "Id"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, retorno.Email),
                        new Claim("usuarioId", usuarioId),
                        new Claim("permissoes", permissoes)
                    }
                );

                var jwt = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = _tokenConfiguration.Issuer,
                    Audience = _tokenConfiguration.Audience,
                    SigningCredentials = _signingConfigurations.SigningCredentials,
                    Subject = claims,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });

                retorno.Token = handler.WriteToken(jwt);

                return StatusCode((int)HttpStatusCode.Created, retorno);
            }
            catch (FestPayUnauthorizedException e)
            {
                return StatusCode((int)HttpStatusCode.Unauthorized, e.Message);
            }
            catch (FestPayBaseException e)
            {
                return StatusCode((int)HttpStatusCode.Unauthorized, e.Message);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.Unauthorized, e.Message);
            }
        }
    }
}