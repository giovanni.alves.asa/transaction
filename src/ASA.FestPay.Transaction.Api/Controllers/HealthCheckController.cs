﻿using System.Net;
using ASA.FestPay.Transaction.Api.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ASA.FestPay.Transaction.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController : ControllerBase
    {
        private IConfiguration _configuration;

        public HealthCheckController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var version = _configuration.GetValue<string>("Version");

            return StatusCode((int)HttpStatusCode.OK, new RetornoApi<string>(version));
        }
    }
}