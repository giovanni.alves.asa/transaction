﻿namespace ASA.FestPay.Transaction.Api.Responses
{
    public class RetornoApi
    {
        public string Mensagem { get; set; }

        public RetornoApi(string mensagem)
        {
            this.Mensagem = mensagem;
        }
    }

    public class RetornoApi<T>
    {
        public string Mensagem { get; set; }

        public T Dados { get; set; }

        public RetornoApi(T dados, string mensagem = null)
        {
            this.Mensagem = mensagem;
            this.Dados = dados;
        }
    }
}